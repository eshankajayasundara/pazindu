import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:image_picker/image_picker.dart';

class addUtility extends StatefulWidget {
  @override
  _addUtilityState createState() => _addUtilityState();
}

class _addUtilityState extends State<addUtility> {
  Color white = Colors.white;
  Color black = Colors.black;
  Color grey = Colors.grey;

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController utilityNameController = TextEditingController();

  TextEditingController utilityPriceController = TextEditingController();

  TextEditingController utilitydescriptionController = TextEditingController();

  File _Image1;
  File _Image2;
  File _Image3;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        //remove shadow in the up
        elevation: 0.1,
        title: Text("Add New Utility"),
        backgroundColor: Colors.blue[900],

      ),
      endDrawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            //      header
            new UserAccountsDrawerHeader(
              accountName: Text('Thamashi Kotuwegedara'),
              accountEmail: Text('thamashi97@gmail.com'),
              currentAccountPicture: GestureDetector(
                child: new CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(Icons.person, color: Colors.white,),
                ),
              ),
              decoration: new BoxDecoration(
                  color: Colors.blue[900]
              ),
            ),
            //          body




            InkWell(
              onTap: (){
                 Navigator.push(context, MaterialPageRoute(builder: (context) => new addUtility()));
              },
              child: ListTile(
                title: Text('Add New Utilities'),
                leading: Icon(Icons.add,color: Colors.yellow,size: 30.0,),
              ),
            ),

            Divider(),

            InkWell(
              onTap: (){
                //Navigator.push(context, MaterialPageRoute(builder: (context) => new UtilityCart()));
              },
              child: ListTile(
                title: Text('Update Utilities'),
                leading: Icon(Icons.update,color: Colors.green,size: 30.0,),
              ),
            ),

            Divider(),


            InkWell(
              onTap: (){},
              child: ListTile(
                title: Text('Delete Utilities'),
                leading: Icon(Icons.delete_forever, color: Colors.red,size: 30.0,),
              ),
            )
          ],
        ),
      ),

      body: Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: new Text("",style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold,fontStyle: FontStyle.italic),),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: OutlineButton(
                      borderSide: BorderSide(color: grey.withOpacity(0.5), width: 2.5),
                      onPressed: (){
                        _selectImage(ImagePicker.pickImage(source: ImageSource.gallery), 1);
                      },
                      child: _displayChild1()
                    ),
                  ),
                ),

                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlineButton(
                      borderSide: BorderSide(color: grey.withOpacity(0.5), width: 2.5),
                      onPressed: (){
                        _selectImage(ImagePicker.pickImage(source: ImageSource.gallery), 2);
                      },
                      child:  _displayChild2()
                    ),
                  ),
                ),

                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlineButton(
                      borderSide: BorderSide(color: grey.withOpacity(0.5), width: 2.5),
                      onPressed: (){
                        _selectImage(ImagePicker.pickImage(source: ImageSource.gallery), 3);
                      },
                      child: _displayChild3()
                    ),
                  ),
                )
              ],
            ),

            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text('Enter the Utility Name with 10 characters at maximum', textAlign: TextAlign.center, style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold)),
            ),

            Padding(
              padding: const EdgeInsets.all(15.0),
              child: TextFormField(
                controller: utilityNameController,
                decoration: InputDecoration(
                    hintText: "Utility Name"
                ),

                validator: (value){
                  if(value.isEmpty){
                    return 'Enter the Utiltiy Item name';
                  } else if(value.length > 10){
                    return 'Utility Name cannot have more than 10 letters';
                  }
                },
              ),
            ),
            //description
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: TextFormField(
                controller: utilitydescriptionController,
                decoration: InputDecoration(
                    hintText: "Utility description"
                ),

                validator: (value){
                  if(value.isEmpty){
                    return 'Enter the Utiltiy Item description';
                  } else if(value.length > 100){
                    return 'Utility description cannot have more than 100 letters';
                  }
                },
              ),
            ),

            ///=====price form
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: TextFormField(
                controller: utilityPriceController,
                //only allowing numbers
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintText: "Utility price"
                ),
                validator: (value){
                  if(value.isEmpty){
                    return 'Enter the Utiltiy Item Price';
                  }
                },
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(20.0),
              child: FlatButton(
                color: Colors.blue[900],
                  textColor: white,
                  onPressed: (){},
                  child: Text("Add Utility", style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16.0),)),
            )
          ],
        ),
      ),
    );
  }

  void _selectImage(Future<File> pickImage, int imageNumber) async{
    File tempImg = await pickImage;
    switch(imageNumber){
      case 1:  setState(() => _Image1 = tempImg);
      break;
      case 2:  setState(() => _Image2 = tempImg);
      break;
      case 3:  setState(() => _Image3 = tempImg);
      break;
    }
   
  }

  Widget _displayChild1() {
    if(_Image1 == null){
     return Padding(
        padding: const EdgeInsets.fromLTRB(14, 40, 14, 40),
        child: new Icon(Icons.add, color: grey,),
      );
    } else{
      return Padding(
        padding: const EdgeInsets.fromLTRB(14, 40, 14, 40),
        child: Image.file(_Image1, fit: BoxFit.fill, width: double.infinity,),
      );
    }
  }

  Widget _displayChild2() {
    if(_Image2 == null){
      return Padding(
        padding: const EdgeInsets.fromLTRB(14, 40, 14, 40),
        child: new Icon(Icons.add, color: grey,),
      );
    } else{
      return Padding(
        padding: const EdgeInsets.fromLTRB(14, 40, 14, 40),
        child: Image.file(_Image2, fit: BoxFit.fill, width: double.infinity,),
      );
    }
  }

  Widget _displayChild3() {
    if(_Image3 == null){
      return Padding(
        padding: const EdgeInsets.fromLTRB(14, 40, 14, 40),
        child: new Icon(Icons.add, color: grey,),
      );
    } else{
      return Padding(
        padding: const EdgeInsets.fromLTRB(14, 40, 14, 40),
        child: Image.file(_Image3, fit: BoxFit.fill, width: double.infinity,),
      );
    }
  }
}