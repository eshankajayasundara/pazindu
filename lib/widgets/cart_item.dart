import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smart_hostel_management_system/widgets/cart_model.dart';


class CartUtl extends StatelessWidget {
  //id for a card
  final String id;
  //id for the specific product
  final String utilityID;
  final double price;
  final int quantity;
  final String name;
//  final double total;

  CartUtl(
    this.id,
    this.utilityID,
    this.price,
    this.quantity,
    this.name,
   // this.total
);
  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: ValueKey(id),
      direction: DismissDirection.endToStart,
      background: Container(
        color: Colors.black26,
      ),
      onDismissed: (direction){
        Provider.of<Cart>(context,listen: false).removeItem(utilityID);
      },
      child: Card(
        child: ListTile(
          leading: CircleAvatar(
            child: FittedBox(
              child: Text('\Rs.$price'),
            ),
          ),
          title: Text(name),
          subtitle: Text('Total:\Rs.${(price*quantity)}'),
          trailing: Text('$quantity x'),

        ),
      ),
    );


  }
}
