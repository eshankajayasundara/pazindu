import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class CartItem{
  final String id;
  final String name;
  final int quantity;
  final double price;

  CartItem({
    @required this.id,
    @required this.name,
    @required this.quantity,
    @required this.price,

});
}

class Cart with ChangeNotifier{
  Map<String ,CartItem> _items = {};

  Map<String, CartItem> get items {
    return{..._items};
  }
  //getting the item count
  int get itemCount{
    return _items.length;
  }
  //adding items to the cart
  void addItem(String utlid,String name,double price){
    if(_items.containsKey(utlid)){
      _items.update(utlid, (existingCartItem) =>
          CartItem(id: DateTime.now().toString(), name: existingCartItem.name,
          quantity: existingCartItem.quantity +1, price: existingCartItem.price));
    }
    else{
      _items.putIfAbsent(utlid, () => CartItem(
        name: name,
        id:DateTime.now().toString(),
        quantity: 1,
        price: price,
      ));
    }
    //notify the changes to the cart
    notifyListeners();
  }

  //remove an item from the cart
  void removeItem(String id){
    _items.remove(id);
    notifyListeners();
  }
   void removeSingleItem(String id){
    if(!_items.containsKey(id)){
      return;
    }if(_items[id].quantity > 1) {
      _items.update(id, (existingCartItem) =>
          CartItem(id: DateTime.now().toString(),
              name: existingCartItem.name,
              quantity: existingCartItem.quantity - 1,
              price: existingCartItem.price));
    }
    notifyListeners();
  }

  //method to remove all the items in the cart
  void clear(){
    _items = {};
    notifyListeners();
  }

}