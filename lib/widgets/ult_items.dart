import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_hostel_management_system/Student/utl_detail_screen.dart';
import 'package:provider/provider.dart';
import 'package:smart_hostel_management_system/widgets/cart_model.dart';
import 'package:smart_hostel_management_system/widgets/utility_models.dart';


class Utl_Items extends StatelessWidget {
  final String name;
  final String img;
  final double price;

  Utl_Items({
    this.name,
    this.img,
    this.price
});

  @override
  Widget build(BuildContext context) {
    final utl = Provider.of<utilities>(context);
    final cart = Provider.of<Cart>(context);
    //to get an ontap.By clicking this we are passing the name of the utility that we selected and navigate to the add to cart page.
    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed(DetailPage.routeName,arguments: utl.utility_id);
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: GridTile(
          child: Image.asset(img),
          footer: GridTileBar(
            title: Text(name),
          //  subtitle: Text(price),
            trailing: IconButton(icon: Icon(Icons.shopping_cart),
            onPressed: (){
              Scaffold.of(context).showSnackBar(SnackBar(
                duration: Duration(seconds: 3),
                content: Text('Item Added to Cart'),
              ));
              cart.addItem(utl.utility_id, utl.utility_name, utl.price);
            }),
            backgroundColor: Colors.black54,
          ),
        ),
      ),
    );
  }
}
