import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class confirmRequest extends StatefulWidget {
  @override
  _confirmRequestState createState() => _confirmRequestState();
}

class _confirmRequestState extends State<confirmRequest> {
  @override
  Widget build(BuildContext context) {
    var _controll;
    return new MaterialApp(
      home: new Scaffold(
        appBar: AppBar(
          title: new Center(child: Text('ITEM DETAILS')),

        ),
        body: Container(
          child: SingleChildScrollView(

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,

              children: <Widget>[

                SizedBox(
                  height: 20.0,
                ),

                Text("Photo"),

                SizedBox(
                  height: 20.0,
                ),

                TextField(
                  controller: _controll,
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide:BorderSide(color: Colors.lightBlue),
                        borderRadius: BorderRadius.all(Radius.circular(30))
                    ),
                  ),
                ),


                SizedBox(
                  height: 150.0,
                ),

                Text("Description"),

                SizedBox(
                  height: 20.0,
                ),

                TextField(
                  controller: _controll,
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide:BorderSide(color: Colors.lightBlue),
                        borderRadius: BorderRadius.all(Radius.circular(30))
                    ),
                  ),
                ),

                SizedBox(
                  height: 80.0,
                ),

                Text("Location"),

                SizedBox(
                  height: 20.0,
                ),

                TextField(
                  controller: _controll,
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide:BorderSide(color: Colors.lightBlue),
                        borderRadius: BorderRadius.all(Radius.circular(30))
                    ),
                  ),
                ),

                SizedBox(
                  height: 50.0,
                ),



                Center(
                  child: FlatButton(
                      onPressed: () {} ,
                      child: Text('CONFIRM'),
                    color: Colors.blue,

                  ),
                )



              ],
            ),
          ),
        ),
      ),
    );

  }
}
