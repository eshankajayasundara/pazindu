import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ApproveRepairItem extends StatefulWidget {
  @override
  _ApproveRepairItemState createState() => _ApproveRepairItemState();
}


class _ApproveRepairItemState extends State<ApproveRepairItem> {

  bool isEditing = false;
  bool textFieldVisibility = false;

  String firestoreCollectionName = "Repair_items";


  getAllItems(){
    return Firestore.instance.collection(firestoreCollectionName).snapshots();
  }


  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: getAllItems(),
      // ignore: missing_return
      builder: (context,snapshot){
        if(snapshot.hasError){
          return Text("Error ${snapshot.error}");
        }
        if(snapshot.hasData){
          print("Documents -> ${snapshot.data.documents.length}");
          return buildList(context,snapshot.data.documents);
        }
      },
    );
  }

}


getAllItems() {
}


Widget buildList(BuildContext context, List<DocumentSnapshot> snapshot){
  return ListView(
    children: snapshot.map((data) => ListItemBuild(context,data)).toList(),
  );
}

Widget ListItemBuild(BuildContext context, DocumentSnapshot data){
  var Item;
  final item = Item.fromSnapshot(data);
  return Padding(

  );

}
