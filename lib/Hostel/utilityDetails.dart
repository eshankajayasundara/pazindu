import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'cart.dart';

class ItemDetail extends StatefulWidget {
  String itemName;
  double itemPrice;
  String itemImage;
  double itemRating;

  ItemDetail.items({
    this.itemName,
    this.itemPrice,
    this.itemImage,
    this.itemRating
  });
  
  @override
  _ItemDetailState createState() => _ItemDetailState();
}

class _ItemDetailState extends State<ItemDetail> {
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: new AppBar(
        title: Text("Utility Details"),
        centerTitle: false,
        backgroundColor: Colors.blue[900],
        iconTheme: IconThemeData(color: Colors.white),
      ),
      
      body: new Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          new Container(
            height: 300.0,
            decoration: new BoxDecoration(
              color: Colors.grey.withAlpha(50),
                image: new DecorationImage(
                    image: new AssetImage(widget.itemImage),fit: BoxFit.fitHeight),
              borderRadius: new BorderRadius.only(
                bottomRight: new Radius.circular(100.0),
                bottomLeft: new Radius.circular(100.0)
              )
            ),
          ),
          new SingleChildScrollView(
            child: new  Column(
              children: <Widget>[
                new SizedBox(
                  height: 50.0,
                ),
                new Card(
                  child: new Container(
                    width: screenSize.width,
                    margin: new EdgeInsets.only(left: 20.0, right: 20.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new SizedBox(
                          height: 10.0,
                        ),
                        new Text(widget.itemName,
                          style: new TextStyle(
                              color: Colors.black,fontWeight: FontWeight.bold,fontSize: 25.0),
                        ),

                        new SizedBox(
                          height: 10.0,
                        ),
                        new Text(widget.itemPrice.toString(),
                          style: new TextStyle(
                              color: Colors.red,fontWeight: FontWeight.bold),
                        ),
                        new SizedBox(
                          height: 10.0,
                        ),

                        new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                new Icon(
                                  Icons.star,
                                  color: Colors.blue[900],
                                  size: 20.0,
                                ),
                                new SizedBox(
                                  height: 20.0,
                                )
                              ],
                            )
                          ],
                        )

                      ],
                    ),
                  ),
                ),
                new Card(
                  child: new Container(
                    width: screenSize.width,
                    height: 150.0,
                    child: new ListView.builder(
                      scrollDirection: Axis.horizontal,
                        itemCount: 5,
                        itemBuilder: (context,index) {
                        return new Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            new Container(
                              margin: new EdgeInsets.only(left: 5.0,right: 5.0),
                              height: 140.0,
                              width: 100.0,
                              child: new Image.asset(widget.itemImage),
                            ),
                            new Container(
                              margin: new EdgeInsets.only(left: 5.0 ,right: 5.0),
                              height: 140.0,
                              width: 100.0,
                              decoration: new BoxDecoration(
                                color: Colors.grey.withAlpha(50)
                              ),
                            )
                          ],
                        );
                        }),

                  ),
                ),
                new Card(
                  child: new Container(
                    width: screenSize.width,
                    margin: new EdgeInsets.only(left: 20.0, right: 20.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new SizedBox(
                          height: 10.0,
                        ),
                        new Text("Description", style: new TextStyle(
                            color: Colors.black,fontWeight: FontWeight.bold,fontSize: 18.0),
                        ),

                        new SizedBox(
                          height: 10.0,
                        ),
                        new Text("My description information", style: new TextStyle(
                            color: Colors.black,fontWeight: FontWeight.w400,fontSize: 14.0),
                        ),
                        new SizedBox(
                          height: 10.0,
                        ),

                      ],
                    ),
                  ),
                ),
                new Card(
                  child: new Container(
                    width: screenSize.width,
                    margin: new EdgeInsets.only(left: 20.0, right: 20.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new SizedBox(
                          height: 10.0,
                        ),
                        new Text("Brand", style: new TextStyle(
                            color: Colors.black,fontWeight: FontWeight.bold,fontSize: 18.0),
                        ),

                        new SizedBox(
                          height: 10.0,
                        ),
                        new SizedBox(
                          height: 50.0,
                          child: new ListView.builder(
                            scrollDirection: Axis.horizontal,
                              itemCount: 4,
                              itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.all(4.0),
                              child: new ChoiceChip(
                                  label: new Text("Brands ${index}"),
                                  selected: false),
                              );
                              }),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
      floatingActionButton: Stack(
        alignment: Alignment.topLeft,
        children: <Widget>[
          new FloatingActionButton(
            onPressed: (){
              Navigator.of(context).push(new CupertinoPageRoute(builder: (BuildContext context) => new Cart()));
              },
            child: new Icon(Icons.shopping_cart),

          ),
          new CircleAvatar(
            radius: 10.0,
            backgroundColor: Colors.red,
            child: new Text('0'),
          )
        ],
      ),

      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: new BottomAppBar(
        color: Colors.blue[900],
        elevation: 0.0,
        shape: new CircularNotchedRectangle(),
        notchMargin: 5.0,
        child: new Container(
          height: 50.0,
          decoration: new BoxDecoration(
            color: Colors.blue[900]),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Container(
                width: (screenSize.width - 20) / 2,
                child: new Text("ADD TO FAVOURITES",
                textAlign: TextAlign.center,
                style: new TextStyle(
                  color: Colors.white,fontWeight: FontWeight.bold
                ),),
              ),
              new Container(
                width: (screenSize.width - 20) / 2 ,
                child: new Text("ORDER NOW",
                textAlign: TextAlign.center,
                  style: new TextStyle(
                      color: Colors.white,fontWeight: FontWeight.bold),
              )
              ),
            ],
          ),

          ),
        ),


    );
  }
}
