import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BookRoom extends StatefulWidget {
  @override
  _BookRoomState createState() => _BookRoomState();
}

enum RoomTypes { singleRoom, doubleSharing, tripleSharing }

class _BookRoomState extends State<BookRoom> {

  hexColor(String colorhexcode){
    String colornew = '0xff' + colorhexcode;
    colornew = colornew.replaceAll('#','');
    int colorint = int.parse(colornew);
    return colorint;
  }

  var roomTypes = ['Single Room','Double Sharing','Triple Sharing'];
  var CurrentRoomTypeSelected = 'Single Room';

  RoomTypes _roomType = RoomTypes.singleRoom;

  Widget _buildDateFromButton(){
    return RaisedButton.icon(
      onPressed: (){
        print('FromDateSelected');
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      icon: Icon(Icons.calendar_today,
        color: Colors.white,),
      label: Text('From',
        style: TextStyle(
          color: Colors.white,
          fontSize: MediaQuery.of(context).size.height/40,
        ),),
      color: Color(hexColor('#0D47A1')),
    );
  }

  Widget _buildDateToButton(){
    return RaisedButton.icon(
      onPressed: (){
        print('ToDateSelected');
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      icon: Icon(Icons.calendar_today,
        color: Colors.white,),
      label: Text('To',
        style: TextStyle(
          color: Colors.white,
          fontSize: MediaQuery.of(context).size.height/40,
        ),),
      color: Color(hexColor('#0D47A1')),
    );
  }




  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Book Room"),
          backgroundColor: Color(hexColor('#0D47A1')),
        ),
        body:SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[

              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: Text('Select the Room type you prefer',style: TextStyle(
                  color: Color(hexColor('#0D47A1')),
                  fontSize: MediaQuery.of(context).size.height/40,
                  fontWeight: FontWeight.w700,
                ),),
              ),


              ListTile(
                title: const Text('Single Room'),
                leading: Radio(
                  value: RoomTypes.singleRoom,
                  groupValue: _roomType,
                  onChanged: (RoomTypes value) {
                    setState(() {
                      _roomType = value;
                    });
                  },
                ),
              ),
              ListTile(
                title: const Text('Single Room Room'),
                leading: Radio(
                  value: RoomTypes.doubleSharing,
                  groupValue: _roomType,
                  onChanged: (RoomTypes value) {
                    setState(() {
                      _roomType = value;
                    });
                  },
                ),
              ),

              ListTile(
                title: const Text('Triple Sharing Room'),
                leading: Radio(
                  value: RoomTypes.tripleSharing,
                  groupValue: _roomType,
                  onChanged: (RoomTypes value) {
                    setState(() {
                      _roomType = value;
                    });
                  },
                ),
              ),

              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: Text('Select the duration of your stay',style: TextStyle(
                  color: Color(hexColor('#0D47A1')),
                  fontSize: MediaQuery.of(context).size.height/40,
                  fontWeight: FontWeight.w700,
                ),),
              ),

              _buildDateFromButton(),
              _buildDateToButton(),


              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: Text('Emergency Contact',
                  style: TextStyle(
                    color: Color(hexColor('#0D47A1')),
                    fontSize: MediaQuery.of(context).size.height/40,
                    fontWeight: FontWeight.w700,
                  ),),
              ),

              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.person),
                    labelText: "Contact Person Name",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(hexColor('#0D47A1')),
                      ),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),

              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.people),
                    labelText: "Relationship with the student",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(hexColor('#0D47A1')),
                      ),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.phone_android),
                    labelText: "Contact Number",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(hexColor('#0D47A1')),
                      ),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),

              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.mail_outline),
                    labelText: "Address",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(hexColor('#0D47A1')),
                      ),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),

              Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0 , horizontal: 16.0),
                child: RaisedButton(
                  onPressed: (){
                    print('Submitted');
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),

                  child: Text('Submit',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: MediaQuery.of(context).size.height/40,
                      fontWeight: FontWeight.w700,
                    ),),
                  color: Color(hexColor('#0D47A1')),
                ) ,
              )
            ],
          ),
        ),

      ),
    );
  }

}
