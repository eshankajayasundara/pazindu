import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Registration extends StatefulWidget {
  @override
  _RegistrationState createState() => _RegistrationState();
}
enum UserType { student, owner, maintainanceManager }
class _RegistrationState extends State<Registration> {

  var userTypes = ['Student','Owner','Maintainance Manager'];
  var CurrentUserTypeSelected = 'Student';


  hexColor(String colorhexcode){
    String colornew = '0xff' + colorhexcode;
    colornew = colornew.replaceAll('#','');
    int colorint = int.parse(colornew);
    return colorint;
  }
  UserType _userType = UserType.student;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Registration"),
          backgroundColor: Color(hexColor('#0D47A1')),
        ),
        body:SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 30.0,
              ),
              CircleAvatar(
                backgroundColor:Color(hexColor('#0D47A1')),
                radius: 90.0,
              ),

              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: Text('Upload a Photo for ImageID',style: TextStyle(
                  color: Color(hexColor('#0D47A1')),
                  fontSize: MediaQuery.of(context).size.height/40,
                  fontWeight: FontWeight.w400,
                ),),
              ),



              Padding(
                padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 40.0),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                      child: RaisedButton.icon(
                        onPressed:(){},
                        icon: Icon(Icons.photo_camera,
                            color: Colors.white),
                        label: Text('Camera',
                          style: TextStyle(
                            color: Colors.white,
                          ),),
                        color: Color(hexColor('#0D47A1')),
                        padding: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                      child:RaisedButton.icon(
                        onPressed: (){},
                        icon: Icon(Icons.phone_android,
                          color: Colors.white,),
                        label: Text('Phone gallery',
                          style: TextStyle(
                            color: Colors.white,),),
                        color: Color(hexColor('#0D47A0')),
                        padding: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
                      ),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: Divider(
                  thickness: 1.0,
                  color: Color(hexColor('#0D47A1')),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.person),
                    labelText: "Full Name",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(hexColor('#0D47A1')),
                      ),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),

              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.phone),
                    labelText: "Contact Number",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(hexColor('#0D47A1')),
                      ),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.email),
                    labelText: "E-mail Address",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(hexColor('#0D47A1')),
                      ),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.credit_card),
                    labelText: "NIC Number",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(hexColor('#0D47A1')),
                      ),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),

              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.vpn_key),
                    labelText: "Password",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(hexColor('#0D47A1')),
                      ),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.vpn_key),
                    labelText: "Re-type Password",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(hexColor('#0D47A1')),
                      ),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),

              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: Text('Select the User Type',style: TextStyle(
                  color: Color(hexColor('#0D47A1')),
                  fontSize: MediaQuery.of(context).size.height/40,
                  fontWeight: FontWeight.w300,
                ),),
              ),


              ListTile(
                title: const Text('Student'),
                leading: Radio(
                  value: UserType.student,
                  groupValue: _userType,
                  onChanged: (UserType value) {
                    setState(() {
                      _userType = value;
                    });
                  },
                ),
              ),
              ListTile(
                title: const Text('Owner'),
                leading: Radio(
                  value: UserType.owner,
                  groupValue: _userType,
                  onChanged: (UserType value) {
                    setState(() {
                      _userType = value;
                    });
                  },
                ),
              ),

              ListTile(
                title: const Text('Maintainance Manager'),
                leading: Radio(
                  value: UserType.maintainanceManager,
                  groupValue: _userType,
                  onChanged: (UserType value) {
                    setState(() {
                      _userType = value;
                    });
                  },
                ),
              ),



              Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0 , horizontal: 16.0),
                child: RaisedButton(
                  onPressed: (){
                    print('Submitted');
                  },
                  child: Text('Submit',
                    style: TextStyle(
                      color: Colors.white,
                    ),),
                  color: Color(hexColor('#0D47A1')),
                ) ,
              )
            ],
          ), 
        ),

      ),
    );
  }

}
