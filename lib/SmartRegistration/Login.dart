import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_hostel_management_system/SmartRegistration/Registration.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  String loginID, password;




  hexColor(String colorhexcode){
    String colornew = '0xff' + colorhexcode;
    colornew = colornew.replaceAll('#','');
    int colorint = int.parse(colornew);
    return colorint;
  }
  Widget _buildTitle(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text('Welcome',
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.height/25,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
      ],
    );
  }

  Widget _buildLoginIDRow(){

    return Padding(
      padding: EdgeInsets.all(8),
      child: TextFormField(
        keyboardType: TextInputType.text,
        onChanged: (value){
          setState(() {
            loginID = value;
          });
        },
        decoration: InputDecoration(
          prefixIcon: Icon(
            Icons.person,
            color: Color(hexColor('#0D47A1')),
          ),
          labelText: 'Login ID',

        ) ,
      ),
    );
  }






  Widget _buildPasswordRow(){

    return Padding(
      padding: EdgeInsets.all(8),
      child: TextFormField(
        keyboardType: TextInputType.text,
        obscureText: true,
        onChanged: (value){
          setState(() {
            password = value;
          });
        },
        decoration: InputDecoration(
          prefixIcon: Icon(
            Icons.lock,
            color: Color(hexColor('#0D47A1')),
          ),
          labelText: 'Password',

        ) ,
      ),
    );
  }






  Widget _buildLoginButton(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          height: (MediaQuery.of(context).size.height/20),
          width: 5*(MediaQuery.of(context).size.width/20),
          margin: EdgeInsets.only(bottom: 20),

          child: RaisedButton(
            color: Color(hexColor('#0D47A1')),
            elevation: 5.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            onPressed: (){},

            child: Text('Login',
                style: TextStyle(
                  color: Colors.white70,
                  letterSpacing: 1.5,
                  fontSize: MediaQuery.of(context).size.height/40,
                )),
          ),
        )
      ],
    );
  }


  Widget _buildRegisterButton(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          height: (MediaQuery.of(context).size.height/20),
          width: 6*(MediaQuery.of(context).size.width/20),
          margin: EdgeInsets.only(bottom: 20),

          child: RaisedButton(
            color: Color(hexColor('#0D47A1')),
            elevation: 5.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => new Registration()));
            },

            child: Text('Register',
                style: TextStyle(
                  color: Colors.white70,
                  letterSpacing: 1.5,
                  fontSize: MediaQuery.of(context).size.height/40,
                )),
          ),
        )
      ],
    );
  }

  Widget _buildORwraw(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(bottom: 20),
          child: Text('- OR -',
            style: TextStyle(
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }



  Widget _buildContainer(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(20),
          ),
          child: Container(
            height: MediaQuery.of(context).size.height*0.6,
            width: MediaQuery.of(context).size.width*0.8,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Login",
                        style: TextStyle(
                          fontSize: MediaQuery.of(context).size.height/30,
                          color: Color(hexColor('#0D47A1')),
                        )
                    )

                  ],
                ),

                _buildLoginIDRow(),
                _buildPasswordRow(),
                _buildLoginButton(),
                _buildORwraw(),
                _buildRegisterButton(),

              ],
            ),
          ),
        )
      ],
    );
  }




  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white70,
        body: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height*0.7,
              width: MediaQuery.of(context).size.width,
              child: Container(
                decoration: BoxDecoration(
                    color: Color(hexColor('#0D47A1')),
                    borderRadius: BorderRadius.only(
                        bottomLeft: const Radius.circular(70),
                        bottomRight: const Radius.circular(70,)
                    )
                ),
              ),
            ),

            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _buildTitle(),
                _buildContainer(),

              ],

            )



          ],
        ),
      ),
    );
  }
}

