import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:smart_hostel_management_system/Student/StudentHomePage.dart';
import 'package:smart_hostel_management_system/Student/utl_detail_screen.dart';
import 'package:smart_hostel_management_system/widgets/cart_model.dart';
import 'package:smart_hostel_management_system/widgets/utility_models.dart';
import 'package:smart_hostel_management_system/Student/cart_screen.dart';




class StudentHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: UtilityItems(),
        ),

        ChangeNotifierProvider.value(value: Cart(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Utility Facilities',
        theme: ThemeData(
          primaryColor: Colors.blue[900],
          accentColor: Colors.white
        ),
        home: StudentHomePage(),
        routes: {
          DetailPage.routeName: (ctx)=>DetailPage(),
          CartScreen.routeName: (ctx) => CartScreen(),
        },
      ),
    );
  }
}
