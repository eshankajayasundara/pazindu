import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_hostel_management_system/widgets/cart_model.dart';
import 'package:smart_hostel_management_system/widgets/utility_models.dart';
import 'package:provider/provider.dart';


class DetailPage extends StatelessWidget {
  static const routeName = '/utility-detail';

  @override
  Widget build(BuildContext context) {
    final utilityID = ModalRoute.of(context).settings.arguments as String;
    final loadedUtl = Provider.of <UtilityItems> (context).findById(utilityID);
    final cart = Provider.of<Cart>(context);

    return Scaffold(
      appBar: AppBar(
       title: Text(loadedUtl.utility_name),
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: 250,
            width: double.infinity,
            child: Padding(padding: const EdgeInsets.all(10),
            child: Image.asset(loadedUtl.img),),
          ),

          Text('\Rs.${loadedUtl.price} per month', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0,color: Colors.red),),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Text('${loadedUtl.description}',style: TextStyle(fontSize: 20.0),),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          cart.addItem(utilityID, loadedUtl.utility_name, loadedUtl.price);
        },
      child: Icon(Icons.shopping_cart,size: 30,color: Colors.white,),backgroundColor: Colors.blue[900],),
    );
  }
}
