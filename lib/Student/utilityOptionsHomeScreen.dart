import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_hostel_management_system/Hostel/HostelMainHomepage.dart';

import 'package:smart_hostel_management_system/Student/StudentHome.dart';
import 'package:smart_hostel_management_system/manageUtiltiy/admin_home.dart';
import 'package:smart_hostel_management_system/manageUtiltiy/add_utility.dart';

class utilityHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text("Utility Facilities"),
        /*
        actions: <Widget>[
         IconButton(icon: Icon(Icons.shopping_cart,size: 30,),
            onPressed: () => Navigator.of(context).pushNamed(CartScreen.routeName),
          )
        ],
        */
      ),

      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            //      header
            new UserAccountsDrawerHeader(
              accountName: Text('Thamashi Kotuwegedara'),
              accountEmail: Text('thamashi97@gmail.com'),
              currentAccountPicture: GestureDetector(
                child: new CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(Icons.person, color: Colors.white,),
                ),
              ),
              decoration: new BoxDecoration(
                  color: Colors.blue[900]
              ),
            ),
            //          body

            InkWell(
              onTap: (){

              },
              child: ListTile(
                title: Text('Home Page'),
                leading: Icon(Icons.home,color: Colors.red, size: 30.0,),
              ),
            ),

            InkWell(
              onTap: (){},
              child: ListTile(
                title: Text('My Account'),
                leading: Icon(Icons.person,color: Colors.black, size: 30.0,),
              ),
            ),

            InkWell(
              onTap: (){},
              child: ListTile(
                title: Text('My Orders'),
                leading: Icon(Icons.shopping_basket,color: Colors.blueAccent,size: 30.0),
              ),
            ),

            InkWell(
              onTap: (){

              },
              child: ListTile(
                title: Text('Shopping Cart',),
                leading: Icon(Icons.shopping_cart,color: Colors.blueAccent,size: 30.0),
              ),
            ),

            Divider(),
            InkWell(
              onTap: (){},
              child: ListTile(
                title: Text('Settings'),
                leading: Icon(Icons.settings,color: Colors.black,size: 30.0),
              ),
            ),

            InkWell(
              onTap: (){},
              child: ListTile(
                title: Text('About'),
                leading: Icon(Icons.help, color: Colors.green,size: 30.0),
              ),
            )
          ],
        ),
      ),

      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: 250.0,
                    width: double.infinity,
                    color: Colors.blue[900],
                  ),

                  Positioned(
                    bottom: 50.0,
                    right:100.0,
                    child: Container(
                      height: 400.0,
                      width: 400.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(200.0),
                        color: Colors.indigoAccent.withOpacity(0.5),

                      ),
                    ),
                  ),

                  Positioned(
                    bottom: 100.0,
                    left:150.0,
                    child: Container(
                      height: 300.0,
                      width: 300.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(200.0),
                        color: Colors.indigoAccent.withOpacity(0.5),

                      ),
                    ),
                  ),

                  Positioned(
                    top: 55.0,
                    left: 15.0,
                    child: Text('Utility Options', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 40.0,color: Colors.white),),
                  ),
                  Positioned(
                    top:75.0,
                    child: Column(
                      children: <Widget>[

                      ],
                    ),
                  )
                ],

              )
            ],

          ),

          new Padding(padding: const EdgeInsets.all(15.0),
            child: new Text(' Choose your option',
              style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold,color: Colors.blue[900], ),),),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: new FlatButton(color: Colors.grey,
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => new StudentHome()));
                },
                child: Text('Would you like to purchase a utility?',style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),)),
          ),

          Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Text('OR', textAlign: TextAlign.center,
              style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic,),),
          ),

          Padding(
            padding: const EdgeInsets.all(10.0),
            child: new FlatButton(onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => new mainHostelPage()));
            },
                color: Colors.grey,
                child: Text('Did you bring your own utilities?',style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold,),)),
          )



        ],


      ),
    );
  }
}

