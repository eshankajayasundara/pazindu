import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smart_hostel_management_system/widgets/cart_model.dart';
import 'package:smart_hostel_management_system/widgets/cart_item.dart';

class CartScreen extends StatelessWidget {
  static const routeName = '/cart';

  @override
  Widget build(BuildContext context) {

    final cart = Provider.of<Cart>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('My Cart',
          style: TextStyle(fontSize: 30.0,fontWeight: FontWeight.bold,color: Theme.of(context).accentColor),),
      ),

      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
                itemCount: cart.items.length,
                itemBuilder: (ctx,i ) => CartUtl(
                  cart.items.values.toList()[i].id,
                  cart.items.keys.toList()[i],
                  cart.items.values.toList()[i].price,
                  cart.items.values.toList()[i].quantity,
                  cart.items.values.toList()[i].name
                )),
          ),


         Padding(
           padding: const EdgeInsets.all(15.0),
           child: new  FlatButton(onPressed: (){},color: Colors.blue[900],
                child: Text('Purchase',style: TextStyle(fontSize: 20.0,color: Colors.white),)),
         )
        ],
      ),
    );
  }
}
