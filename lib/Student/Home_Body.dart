import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:smart_hostel_management_system/widgets/utilityProducts.dart';
class HomeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 250.0,
                  width: double.infinity,
                  color: Colors.blue[900],
                ),

                Positioned(
                  bottom: 50.0,
                  right:100.0,
                  child: Container(
                    height: 400.0,
                    width: 400.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(200.0),
                      color: Colors.indigoAccent.withOpacity(0.5),

                    ),
                  ),
                ),

                Positioned(
                  bottom: 100.0,
                  left:150.0,
                  child: Container(
                    height: 300.0,
                    width: 300.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(200.0),
                      color: Colors.indigoAccent.withOpacity(0.5),

                    ),
                  ),
                ),

                Positioned(
                  top: 55.0,
                  left: 15.0,
                  child: Text('Allowed Utilities', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 40.0,color: Colors.white),),
                ),
                Positioned(
                  top:75.0,
                  child: Column(
                    children: <Widget>[

                    ],
                  ),
                )
              ],

            )
          ],

        ),

        new Padding(padding: const EdgeInsets.all(15.0),
          child: new Text('Available utilities',
            style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold,color: Colors.blue[900], ),),),

        AllUtilities()


      ],


    );
  }
}
