import 'package:flutter/material.dart';
import 'package:smart_hostel_management_system/e_pages/owner_dashboard.dart';
import 'package:smart_hostel_management_system/e_pages/student_dashboard.dart';
import 'package:smart_hostel_management_system/SmartRegistration/Login.dart';

import 'package:cloud_firestore/cloud_firestore.dart';


void main() => runApp(MaterialApp(
  initialRoute: '/o_dashboard/s_dashboard',
  routes: {
    '/': (context) => Login(),
    '/o_dashboard': (context) => O_Dashboard(),
    '/o_dashboard/s_dashboard': (context) => S_Dashboard()

  },
));

