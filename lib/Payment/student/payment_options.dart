import 'package:flutter/material.dart';
//import 'package:payment/owner/payment_history.dart';
//import 'package:payment/student/credit_card.dart';
//import 'package:payment/student/photo_upload.dart';
import 'package:smart_hostel_management_system/Payment/owner/payment_history.dart';
import 'package:smart_hostel_management_system/Payment/student/photo_upload.dart';
import 'credit_card.dart';
import 'home_page.dart';

class PaymentOptions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Payment Options"),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.pop(context,MyHomePage());
            }
            ),
        actions: <Widget>[
          IconButton(
//            padding:EdgeInsets.only(right: 30.0),
              icon: Icon(Icons.home),
              onPressed: (){

              }
          ),
        ],
      ),
      body:  Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Payment Options',
            ),
            RaisedButton(
              onPressed: (){
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_){
                      return CreditCard();
                    }
                ));
              },
              color: Colors.amber,
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Icon(Icons.add),
                  Text("Online Banking",
                  style: TextStyle(color: Colors.black),)
                ],
              ),
            ),
            Divider(),
            RaisedButton(
              onPressed: (){
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_){
                      return PhotoUpload();
                    }
                ));
              },
              color: Colors.amber,
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Icon(Icons.add),
                  Text("Upload Bank Slip",
                    style: TextStyle(color: Colors.black),)
                ],
              ),
            ),
            Divider(),
            RaisedButton(
              onPressed: (){
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_){
                      return PaymentHistory();
                    }
                ));
              },
              color: Colors.redAccent,
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Icon(Icons.add),
                  Text("OWNER",
                    style: TextStyle(color: Colors.black),)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
