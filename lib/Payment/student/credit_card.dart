import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:payment/student/payment_options.dart';
import 'package:smart_hostel_management_system/Payment/student/payment_options.dart';
import 'home_page.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

class CreditCard extends StatefulWidget {
  @override
  _CreditCardState createState() => _CreditCardState();
}

class _CreditCardState extends State<CreditCard> {
  int total = 0;
  Razorpay _razorpay;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _razorpay.clear();
  }

  void openCheckout() async{
    var options = {
      "key" : "rzp_test_uFKthTcC7tGWTc",
      "amount": total*1000,
      "name" : "Hostel Management System",
      "description" : "Test payment",
      "prefill" : {
        "contact" : "",
        "email" : "",
      },
      "external" : {
        "wallets" : ["paytm"]
      },
    };

    try{
      _razorpay.open(options);
    }
    catch(e){
      debugPrint(e);
    }
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
  Fluttertoast.showToast(msg: "Success" + response.paymentId);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Fluttertoast.showToast(msg: "ERROR" + response.code.toString() + " + " +response.message);  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(msg: "External Wallet" + response.walletName);  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Credit Card Details"),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.pop(context,PaymentOptions());
            }
        ),
        actions: <Widget>[
          IconButton(
//            padding:EdgeInsets.only(right: 30.0),
              icon: Icon(Icons.home),
              onPressed: (){
//                Navigator.of(context)..push(MaterialPageRoute(
//                    builder: (_){
//                      return MyHomePage();
//                    }));
              }
          ),
        ],
      ),
      body: Center(
        child: Container(
          margin: EdgeInsets.only(left:20.0,right: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children:<Widget> [
              LimitedBox(
                maxWidth: 80.0,
                child: TextField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: "Please Enter the Amount"
                  ),
                  onChanged: (value){
                    setState(() {
                      total = num.parse(value);
                    });
                  },
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              RaisedButton(
                child: Text("Take payment",
                style: TextStyle(
                  color: Colors.pink,
                ),
                ),
                  onPressed: (){
                openCheckout();
              })
            ],
          ),
        ),
      ),
    );
  }
}
