import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'payment_options.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({this.title});

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MY Home Page"),
      ),
      body: Container(
        child: ListView(
          padding: EdgeInsets.only(bottom: 100.0),
          children: <Widget>[
            Center(
              child: Container(
                margin: const EdgeInsets.all(1.0),
                color: Colors.yellow,
                height: 100,
                child: ClipRect(
                  child: Banner(
                    message: "Updated",
                    location: BannerLocation.topEnd,
                    color: Colors.red,
                    child: Container(
                      color: Colors.lightBlue,
                      height: 100,
                      child: Center(
                        child: Text(
                          "Total Fees\t =\t Rs.10 000.00",
                          style: TextStyle(
                            fontSize: 21.0,
                              fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(20.0),
              child: Text(
                "Boarding Fees",
                style: TextStyle(
                  fontSize: 21.0,
                ),
              ),
            ),
            Divider(),
            ListTile(
              title: Text("01/01/2020 - 02/02/2020"),
              trailing: Text("Rs 5000.00"),
            ),
            Divider(),
            ListTile(
              title: Text("01/01/2020 - 02/02/2020"),
              trailing: Text("Rs 5000.00"),
            ),
            Divider(),
            ListTile(
              title: Text("01/01/2020 - 02/02/2020"),
              trailing: Text("Rs 5000.00"),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.all(20.0),
              child: Text("Utility Fees",
                style: TextStyle(fontSize: 21.0,),
              ),
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.tv),
              title: Text("Television"),
              trailing: Text("Rs 5000.00"),
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.wifi),
              title: Text("WIFI"),
              trailing: Text("Rs 5000.00"),
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.hot_tub),
              title: Text("Heater"),
              trailing: Text("Rs 5000.00"),
            ),
            Divider(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(
            builder: (_){
              return PaymentOptions();
            }
          ),
          );
        },
        label: Text("Pay Now"),
        icon: Icon(Icons.navigate_next),
        backgroundColor: Color(0xFF0d47a1),
      ),
//      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,// This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}